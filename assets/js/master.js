var shopViews = angular.module('eShop', ['ngRoute']);

shopViews.run(function($rootScope){
  $rootScope.shopList = [];
});

shopViews.config(['$routeProvider', function($routeProvider){

  $routeProvider

    .when('/home',{
      templateUrl:'partials/home.html',
      controller : 'homeControl'
    })

    .when('/sucre/:id?',{
      templateUrl:'partials/sucre.html',
      controller:'sucreControl'
    })

    .when('/sale',{
      templateUrl:'partials/sale.html',
      controller : 'saleControl'
    })
    .when('/panier',{
      templateUrl:'partials/panier.html',
      controller : 'panierControl'
    })

    .otherwise({
      redirectTo:'/home'
    });

}]);

shopViews.controller('homeControl',['$scope', '$rootScope', function($scope, $rootScope){

}]);

shopViews.controller('sucreControl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
  $http.get('sucre.json').then(function(response){
    $scope.articles = response.data;
  }, function(response){
    $scope.articles = "Quelque chose cloche !"
  });
  var panier=[];
  $scope.ajoutPanier=function(article){
    $rootScope.shopList.push(article);
    console.log(article.nom);
  }
}]);

shopViews.controller('saleControl', ['$scope', '$http', function($scope, $http){
  $http.get('salee.json').then(function(response){
    $scope.articles = response.data;

  }, function(response){
    $scope.articles = "Quelque chose cloche !"
  });
}]);


shopViews.controller('panierControl', ['$scope', '$rootScope', '$routeParams', function($scope, $rootScope, $routeParams){
 $scope.id=$routeParams.id;
 $scope.article=$rootScope.shopList[$scope.id];
 $scope.toto = $rootScope.shopList
}]);
