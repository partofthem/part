var app = angular.module('MyApp', []);
var prixglobal;

/**
 * controller qui permet de générer plusieurs articles à la fois et de récupere les données du JSON
 */
app.controller('Article', function($scope, $http) {
    $scope.articles = [];

    $http.get('data.json').then(function (reponse) {
        $scope.articles = reponse.data.articles
        var BlockList = document.getElementById('BlockArticles');

        for (var i = 0;i < $scope.articles.length; i++){

            var contenu = "<div class='Article'>" +
                    "<h1>" + $scope.articles[i].Nom + "</h1>"+
                    "<input type='button' value='Commander' onclick='Ajoutpanier(" + $scope.articles[i].id + ", " + $scope.articles[i].prix + ");'/>"+
                "</div>"
            BlockList.innerHTML += contenu;
        }

    });
});

/**
 * permet d'ajouter des articles à ton panier ou d'augmenter la quantité
 * @param idArticle
 */
var panier = [];

function Ajoutpanier(idArticle, prix) {
    var nouveauxArticle = true;

    if (panier.length > 0){
        for (var i = 0; i < panier.length; i++) {
            if (panier[i].id == idArticle) {
                nouveauxArticle = false;
                panier[i].quantite++;
            }
        }
    }
    if(nouveauxArticle){
        panier.push({id: idArticle,
            quantite: 1,
            prix: prix})
    }
    
}

/**
 * renvoie le prix global du panier
 */

function CalculePrixGlobale(){
    for (var i = 0; i > panier.length; i++){
        var prixArticleTotal = 0;
        prixArticleTotal = panier[i].prix * panier[i].quantite;

        prixglobal = prixglobal + prixArticleTotal;
    }
    return prixglobal;
}
