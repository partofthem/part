
foodRama.run(function($rootScope){
  $rootScope.subjectList=[];
  $rootScope.sucréeList=[];
  $rootScope.saléelList=[];
  $rootScope.panierList=[];
});
//configuration des route
foodRama.config(['$routeProvider', function($routeProvider){
  $routeProvider
//quand je click sur form je vais chercher mon fichier form.html et utilise mon controller formControl
  .when('/salée', {
    templateUrl:'partials/salée.html',
    controller:'saléeControl'
  })

  .when('/sucrée/:id?', {
    templateUrl:'partials/sucrée.html',
    controller:'sucréeControl'
  })
  //sinon j'utilise par default form
  .otherwise({
    redirectTo:'/home'
  });
}]);
//Je crée mon controller formControl
foodRama.controller('saléeControl',['$scope', '$rootScope', function($scope, $rootScope){
  //j'appelle ma fonction sendClick du bouton Envoyer
  $scope.sendClick=function(){
    //push permet de récupérer tous les sujets du tableau subjectList
    $rootScope.subjectList.push($scope.subject);
    $rootScope.saléeList.push($scope.salée);
    $rootScope.sucréelList.push($scope.sucrée);
    $rootScope.panierList.push($scope.panier);
  }
}]);
//je crée mon controller viewControl
.controller('viewControl', ['$scope', '$routeParams', '$rootScope', function($scope, $routeParams, $rootScope){
//routeParams récupère toutes les variables de id
  $scope.id=$routeParams.id;
  $scope.subject=$rootScope.subjectList[$scope.id];
  $scope.salée=$rootScope.saléeList[$scope.id];
  $scope.sucrée=$rootScope.sucréeList[$scope.id];
  $scope.panier=$rootScope.panierList[$scope.id];
}]);
